<?php

	namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;
	use yii\db\ActiveRecord;
	use yii\behaviors\BlameableBehavior;
	use yii\helpers\ArrayHelper;	
	
	class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
	{

	public $role; 
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }
	// rules is how the models knows how to assign to the table
	public function rules()
    {
        return  
		[
            [['username', 'password', 'auth_key', ], 'string', 'max' => 255],
			[['firstname', 'lastname', 'phone', ], 'string', 'max' => 255],
			['email', 'email'],		
			[['username', 'password', ], 'required'],
			['role', 'safe'],
			[['created_at', 'updated_at', ], 'integer'],
			[['created_by', 'updated_by', ], 'integer'],
            [['username'], 'unique'],			
        ];				
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
			'firstname' => 'First name',
			'lastname'  => 'Last name',
			'email'  => 'Email',
			'phone'  => 'Phone number',
			'role'  => 'User role',
			'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }	
	

	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }

	public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }

	//getUserole function is to show what is the user role
	public function getUserole()
    {
		$roleArray = Yii::$app->authManager->
					getRolesByUser($this->id);
		$role = array_keys($roleArray)[0];				
		return	$role;
    }
	
	//getCreatedBy function is to show who created the user in the user form, instead of the id
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	
	
	//getUpdateddBy function is to show who created the user in the user form, instead of the id
	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

	//helps to get all users (map from id to full name)
	public static function getUsers()
	{
		$users = ArrayHelper::
					map(self::find()->all(), 'id', 'fullname');
		return $users;						
	}
	
	//return array of all users first names (with "all users" options) - for the search owner dropdown
	public static function getUsersWithAllUsers()
	{
		$users = self::getUsers();
		$users[-1] = 'All Users';
		$users = array_reverse ( $users, true );
		return $users;	
	}	

	public static function getRoles()
	{
		$rolesObjects = Yii::$app->authManager->getRoles();
		$roles = [];
		foreach($rolesObjects as $id =>$rolObj){
			$roles[$id] = $rolObj->name; 
		}
		return $roles; 	
	}
	//return array of all roles (with "all users" options) - for the search roles dropdown
	public static function getRolesWithAllRoles()
	{
		$roles = self::getRoles();
		$roles[-1] = 'All Roles';
		$roles = array_reverse ( $roles, true );
		return $roles;	
	}		

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}

	/**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
		throw new NotSupportedException('You can only login
							by username/password pair for now.');
    }

 
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
         return $this->getAuthKey() === $authKey;
    }	

	public function validatePassword($password)
	{
		return $this->isCorrectHash($password, $this->password); 
	}

	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}
	//method that runs before the save operation
	//in this case checking if the password field has changed, if yes hash again
    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        if ($this->isNewRecord)
		    $this->auth_key = Yii::$app->security->generateRandomString(32);

        return $return;
    }
	
	//method that runs after the save operation
	public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);

        if (!\Yii::$app->user->can('updateUser')){
			return $return;
		}
		$auth = Yii::$app->authManager;
		$roleName = $this->role; 
		$role = $auth->getRole($roleName);
		if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
			$auth->assign($role, $this->id);
		} else {
			$db = \Yii::$app->db;
			$db->createCommand()->delete('auth_assignment',
				['user_id' => $this->id])->execute();
			$auth->assign($role, $this->id);
		}

        return $return;
    }
	
}