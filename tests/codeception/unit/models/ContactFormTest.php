<?php

/*
 namespace tests\codeception\unit\models;
 
 use app\models\ContactForm;
 use Yii;
 use yii\codeception\TestCase;
 use Codeception\Specify;
 
 class ContactFormTest extends TestCase
 {
     use Specify;
 
     protected function setUp()
     {
         Yii::$app->mailer->fileTransportCallback = function ($mailer, $message) {
             return 'testing_message.eml';
         };
     }
 
     protected function tearDown()
     {
         unlink($this->getMessageFile());
         parent::tearDown();
     }
 
     public function testContact()
     {
-        /** @var ContactForm $model */
+        /** @var ContactForm $model */ /*
         $model = $this->getMockBuilder('app\models\ContactForm')
             ->setMethods(['validate'])
             ->getMock();
         $model->expects($this->once())->method('validate')->will($this->returnValue(true));
 
         $model->attributes = [
             'name' => 'Tester',
             'email' => 'tester@example.com',
             'subject' => 'very important letter subject',
             'body' => 'body of current message',
         ];
 
         $this->specify('email should be send', function () use ($model) {
             expect('email should contain user name', $emailMessage)->contains($model->name);
             expect('email should contain sender email', $emailMessage)->contains($model->email);
             expect('email should contain subject', $emailMessage)->contains($model->subject);
             expect('email should contain body', $emailMessage)->contains($model->body);
         });
     }
 
     private function getMessageFile()
     {
         return Yii::getAlias(Yii::$app->mailer->fileTransportPath) . '/testing_message.eml';
     }
 
 }
*/